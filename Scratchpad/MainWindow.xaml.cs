using Fluent;
using Microsoft.Win32;
using System.IO;
using System.Windows;
using System.Windows.Documents;

namespace Scratchpad
{
    public partial class MainWindow : RibbonWindow
    {
        public string FilePath { get; private set; } = "";
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Paste_Click(object sender, RoutedEventArgs e)
        {
            MainEditorArea.Selection.Text = Clipboard.GetText();
        }

        private void Copy_Click(object sender, RoutedEventArgs e)
        {
            MainEditorArea.Copy();
        }

        private void Cut_Click(object sender, RoutedEventArgs e)
        {
            MainEditorArea.Cut();
        }


        private void SelectAll_Click(object sender, RoutedEventArgs e)
        {
            MainEditorArea.SelectAll();
        }

        private void Go_To_Click(object sender, RoutedEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void Replace_Click(object sender, RoutedEventArgs e)
        {
            throw new NotImplementedException();
        }
        private void SaveAs_Click(object sender, RoutedEventArgs e)
        {
            string text = new TextRange(MainEditorArea.Document.ContentStart, MainEditorArea.Document.ContentEnd).Text;
            SetFilePath();
            using FileStream stream = new FileStream(FilePath, FileMode.Create);
            using StreamWriter writer = new StreamWriter(stream);
            writer.Write(text);
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            string text = new TextRange(MainEditorArea.Document.ContentStart, MainEditorArea.Document.ContentEnd).Text;
            if(FilePath == "")
            {
                SetFilePath();
            }
            using FileStream stream = new FileStream(FilePath, FileMode.Create);
            using StreamWriter writer = new StreamWriter(stream);
            writer.Write(text);
        }
        private void SetFilePath()
        {
            SaveFileDialog dialog = new SaveFileDialog
            {
                Filter = "Text Files(*.txt) | *.txt",
                DefaultExt = "txt"
            };
            bool? result = dialog.ShowDialog();
            if (result == true)
            {
                FilePath = dialog.FileName;
            }
            else
            {
                throw new DirectoryNotFoundException("The file was not found");
            }
        }

    }
}