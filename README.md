Scratchpad
---

Scratchpad is an educational project for myself to learn the ins and outs of a C#.Net application with a WPF front end. It will remain free and open source for as long as it is under my maintainership.

![Screenshot](scratchpad.png)